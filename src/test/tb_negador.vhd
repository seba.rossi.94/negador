-- negador.vhd created on 3:35  2018.3.5
library ieee; 
use ieee.std_logic_1164.all; 
library negador_lib;

library vunit_lib;
context vunit_lib.vunit_context;

entity tb_negador is
   generic (runner_cfg : string);
end entity tb_negador;

architecture tb_arq_negador of tb_negador is
component negador
port( 
	X: in std_logic;
	Y: out std_logic
	);  
end component;

signal in_x, out_y: std_logic;

begin

negador_1: entity negador_lib.negador port map (in_x,out_y);  
   main : process
    begin
    test_runner_setup(runner, runner_cfg);
	set_stop_level(failure);
	
	in_x <= '1';	
	wait for 10 ps;
	check_equal(out_y ,'0', result("Error Entrada en 1"));
	
	in_x <= '0';
	wait for 10 ps;
	check_equal(out_y ,'1', result("Error Entrada en 0"));
    
    test_runner_cleanup(runner); -- Simulation ends here
    end process;
end tb_arq_negador;
  
