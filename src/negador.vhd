-- negador.vhd created on 3:35  2018.3.5
library ieee; 
use ieee.std_logic_1164.all; 

entity negador is
	port( 
	X: in std_logic;
	Y: out std_logic
	);
end entity negador;

architecture arq_negador of negador is
  begin
  Y <=  not X;
end arq_negador;
  
