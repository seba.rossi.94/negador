from os.path import join, dirname
from vunit import VUnit

src_path = dirname(__file__)

ui = VUnit.from_argv()
negador_lib = ui.add_library("negador_lib")
negador_lib.add_source_files(join(src_path, "src", "*.vhd"))
negador_lib.add_source_files(join(src_path, "src", "test", "*.vhd"))
ui.main()